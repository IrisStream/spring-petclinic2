def updateEnv(){
	env.BUILD_MACHINE="build-machine"
	env.DEPLOY_MACHINE="deploy-machine"
	env.DOCKER_DIR="/home/iris/docker_src/"
	env.DB_BACKUP_DIR="/home/deploy_user/db_backup/"
	env.SONARQUBE_DIR="/home/build_user/sonarqube_server"
	env.NEXUS_URL="nexus:5000"
	env.DEPLOY_URL="remote-host:8080"
	env.APP_NAME='spring-petclinic'
	env.VERSION= """${sh(
					returnStdout: true,
					script: 'python3 updateVersion.py'
				)}"""
}

def notifySucess(){
	mail to:"ngodaisonn@gmail.com", subject:"SUCCESS: ${currentBuild.fullDisplayName}", body: "Yay, we passed."
}

def notifyFailure(){
	mail to:"ngodaisonn@gmail.com", subject:"FAILURE: ${currentBuild.fullDisplayName}", body: "Boo, we failed."
}

pipeline{
	agent none
	stages{
		stage("Update Environment"){
			agent {label env.BUILD_MACHINE}
			steps{
				script{updateEnv()}
			}
		}
		stage("Build & Test"){
			agent {label env.BUILD_MACHINE}
			stages{
				stage("Build with sonaqube"){
					when {expression{params.BUILD_SRC == true}}
					steps{
						withSonarQubeEnv(installationName: 'spring-petclinic'){
							sh "mvn clean verify sonar:sonar"
						}
					}
				}
				stage("Unit Test"){
					when {expression{params.BUILD_SRC == true}}
					steps{
						sh "mvn test"
					}
				}
				stage("Build docker image"){
					steps{
						sh "./jenkins_scripts/build_docker.sh"
					}
				}
				stage("Push new image to Nexus server"){
					steps{
                        sh "./jenkins_scripts/push_image.sh"
					}
				}
			}
		}
		stage("Deploy"){
			agent {label env.DEPLOY_MACHINE}
            when {expression{params.DEPLOY == true}}
			environment{
				DEPLOY_RESULT = "success"
			}
			stages{
				stage("Backup database & shutdown current version"){
					steps{
						sh "./jenkins_scripts/backup_db.sh"
					}
				}
				stage("Run new version"){
					steps{
						sh "./jenkins_scripts/deploy.sh"
					}
				}
				stage("Check if app run stability"){
					options
					{
						timeout(activity: true, time: 5, unit: 'MINUTES')
					}
					steps{
						sh "./jenkins_scripts/health_check.sh"
					}
				}
				stage("Rollback to previous version"){
					when { expression {env.DEPLOY_RESULT == "fail"}}
					steps{
						sh "./jenkins_scripts/rollback.sh"
					}
				}
			}
		}
	}
	post {
		always {
			script {
				if (currentBuild.result == null) {
				  currentBuild.result = 'SUCCESS'
				}
				script {
					echo "[DONE]"
				}
			}
		}
		changed
		{
			script
			{
				echo "currentBuild ${currentBuild.currentResult}"
				if (currentBuild.currentResult == "FAILURE")
				{
					//Notify Failure
					notifyFailure()
				}
				if (currentBuild.currentResult == "SUCCESS")
				{
					//Notify Success
					notifySucess()
				}
			}
		}
	}
	options
	{
		timeout(activity: true, time: 3, unit: 'HOURS')
	}
}

#!/bin/bash

docker build --build-arg VERSION=${VERSION} -t ${NEXUS_URL}/${APP_NAME}:${VERSION} .

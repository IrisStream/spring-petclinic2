#!/bin/bash

RESPONCE="$(curl -s --head --request GET $DEPLOY_URL | grep 200)"

echo "Responce: $RESPONCE"

if [ -z "$RESPONCE" ] || [ $TEST_ROLLBACK ];
then
        export DEPLOY_RESULT=fail
else
        echo "Health check fine!"
fi

#!/bin/bash


docker push ${NEXUS_URL}/${APP_NAME}:${VERSION}
docker rmi ${NEXUS_URL}/${APP_NAME}:${VERSION}

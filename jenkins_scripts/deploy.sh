#!/bin/bash

echo "docker run -e SPRING_PROFILES_ACTIVE=mysql -p 8080:8080 --name deploy -d ${NEXUS_URL}/${APP_NAME}:${VERSION}"

docker run -e SPRING_PROFILES_ACTIVE=mysql -p 8080:8080 --name deploy -d ${NEXUS_URL}/${APP_NAME}:${VERSION}

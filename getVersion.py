import xml.etree.ElementTree as ET

ET.register_namespace('', "http://maven.apache.org/POM/4.0.0")
tree = ET.parse('pom.xml')
root = tree.getroot()

version=root[3].text

print(version)
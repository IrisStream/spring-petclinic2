import xml.etree.ElementTree as ET

def updateNewVersion(current_version, type='minor'):
	new_version = current_version[:-9]
	version_arr = new_version.split('.')
	version_id = 2
	if type=='major':
		version_id = 1
	version_arr[version_id] = str(int(version_arr[version_id]) + 1)
	new_version = '.'.join(version_arr)
	return f'{new_version}-SNAPSHOT'

ET.register_namespace('', "http://maven.apache.org/POM/4.0.0")
tree = ET.parse('pom.xml')
root = tree.getroot()


version=root[3]

version.text = updateNewVersion(version.text)

tree.write('pom.xml')

print(version.text)
